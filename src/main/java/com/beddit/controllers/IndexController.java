package com.beddit.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Controller
@RequestMapping(value = "/")
public class IndexController {

    @RequestMapping(value = "/getUserList")
    public String getUserList(){
        return "UserList";
    }
}
