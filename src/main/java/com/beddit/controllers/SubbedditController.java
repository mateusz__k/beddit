package com.beddit.controllers;

import com.beddit.models.Subbeddit;
import com.beddit.models.User;
import com.beddit.models.responses.ResponseFactory;
import com.beddit.services.SubbedditService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Date;

@RestController
public class SubbedditController {

    @Autowired
    SubbedditService service;

    private static final Logger LOG = Logger.getLogger(SubbedditController.class);

    @RequestMapping(value = "/testSubbeddit", method = RequestMethod.GET)
    public ResponseEntity<Subbeddit> requestTestSubbeddit() {
        LOG.debug("test subbeddit");
        Subbeddit subbeddit = new Subbeddit();
        subbeddit.setAdmins(Arrays.asList(new User("mati", "1234", "ble@Ble.pl", new Date(1515779405680L))));
        subbeddit.setName("gifs");
        return new ResponseEntity<>(subbeddit, HttpStatus.OK);
    }


    @RequestMapping(value = "/addSubbeddit/{subbedditName}", method = RequestMethod.POST)
    public ResponseEntity addSubbeddit(@PathVariable String subbedditName,
                                       @RequestBody User user) {
        LOG.debug("Adding new subbeddit");
        boolean result = service.addNewSubbeddit(user, subbedditName);

        if(result) {
            LOG.debug("Added new subbeddit");
            return new ResponseEntity<>(ResponseFactory.success(), HttpStatus.OK);
        } else {
            LOG.debug("Failed to create subbeddit");
            return new ResponseEntity<>(ResponseFactory.failed("Failed to create new subbeddit"), HttpStatus.BAD_REQUEST);
        }
    }


}

