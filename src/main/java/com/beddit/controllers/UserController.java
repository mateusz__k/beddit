package com.beddit.controllers;

import com.beddit.models.responses.ResponseFactory;
import com.beddit.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.beddit.models.User;


import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/rest/")
public class UserController {

    private static final String TEST_LOGIN = "test";
    private static final String TEST_PASSWORD = "jdofdsnifodsn";
    private static final String TEST_EMAIL = "ble@ble.pl";
    private static final String INVALID_USER_DATA = "Invalid user data";
    private static final String LOGIN_NOT_UNIQUE = "Login not unique";
    private static final String EMAIL_NOT_UNIQUE = "Email not unique";
    private static final String USER_REGISTER_ERROR = "Error, registering user failed";

    private static final String TEST_USER = "/testUser";
    private static final String GET_ALL_USERS = "/getAllUsers";
    private static final String GET_USER_BY_ID = "/getUserId/{userId}";
    private static final String INVALID_USER_ID = "Invalid user ID";
    private static final String DELETE_USER_BY_ID = "/deleteUser/{userId}";
    private static final String REGISTER_USER = "/registerUser";

    @Autowired
    UserService service;

    @RequestMapping(value = TEST_USER, method = RequestMethod.GET)
    public ResponseEntity<User> requestTestUser() {
        return new ResponseEntity<>(new User(TEST_LOGIN, TEST_PASSWORD, TEST_EMAIL, new Date(1515779405680L)), HttpStatus.OK);
    }

    @RequestMapping(value = REGISTER_USER, method = RequestMethod.POST)
    public ResponseEntity registerUser(@RequestBody User givenUser) {
        Optional<User> optionalUser = Optional.ofNullable(givenUser);

        if (!optionalUser.isPresent())
            return new ResponseEntity<>(ResponseFactory.failed(INVALID_USER_DATA), HttpStatus.BAD_REQUEST);

        User user = optionalUser.get();

        if (user.getEmail() == null || user.getLogin() == null ||
                user.getLogin().length() == 0 || user.getEmail().length() == 0)
            return new ResponseEntity<>(ResponseFactory.failed(INVALID_USER_DATA), HttpStatus.BAD_REQUEST);

        if (!service.isLoginUnique(user.getLogin()))
            return new ResponseEntity<>(ResponseFactory.failed(LOGIN_NOT_UNIQUE), HttpStatus.BAD_REQUEST);

        if (!service.isEmailUnique(user.getEmail()))
            return new ResponseEntity<>(ResponseFactory.failed(EMAIL_NOT_UNIQUE), HttpStatus.BAD_REQUEST);

        boolean registerResult = service.registerUser(user);

        return registerResult ? new ResponseEntity<>(ResponseFactory.success(registerResult), HttpStatus.OK) :
                new ResponseEntity<>(ResponseFactory.failed(USER_REGISTER_ERROR), HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = GET_ALL_USERS, method = RequestMethod.GET)
    public ResponseEntity<List<User>> getAllUsers() {
        return (service.getAllUsers().size() == 0) ?
                new ResponseEntity(ResponseFactory.failed("User list is empty"), HttpStatus.OK) :
                new ResponseEntity(ResponseFactory.success(service.getAllUsers()), HttpStatus.OK);
    }

    @RequestMapping(value = GET_USER_BY_ID, method = RequestMethod.GET)
    public ResponseEntity getUser(@PathVariable Long userId) {
        Optional<User> optionalUser = Optional.ofNullable(service.getUserById(userId));
        return optionalUser.map(user -> new ResponseEntity(ResponseFactory.success(user), HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity(ResponseFactory.failed(INVALID_USER_ID), HttpStatus.BAD_REQUEST));
    }

    @RequestMapping(value = DELETE_USER_BY_ID, method = RequestMethod.GET)
    public ResponseEntity deleteUser(@PathVariable Long userId) {
        return service.removeUser(userId) ?
                new ResponseEntity<>(ResponseFactory.success(), HttpStatus.OK) :
                new ResponseEntity<>(ResponseFactory.failed(INVALID_USER_ID), HttpStatus.BAD_REQUEST);
    }
}
