package com.beddit.controllers;

import com.beddit.models.Post;
import com.beddit.models.User;
import com.beddit.models.responses.ResponseFactory;
import com.beddit.services.PostService;
import com.beddit.services.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class PostController {

    @Autowired
    PostService postService;

    @Autowired
    UserService userService;

    @RequestMapping(value = "/testPost", method = RequestMethod.GET)
    public ResponseEntity<Post> requestTestPost() {
        return new ResponseEntity<>(new Post(), HttpStatus.OK);
    }

    @RequestMapping(value = "/addPost", method = RequestMethod.POST)
    public ResponseEntity addPost(@RequestBody Post post) {
        Optional<User> user = Optional.ofNullable(userService.getUserById(post.getUser().getId()));
        if (user.isPresent() && user.get().getLogin().equals(post.getUser().getLogin())) {
            if (postService.addNewPost(post)) {
                return new ResponseEntity<>(ResponseFactory.success(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(ResponseFactory.failed("Adding post failed"), HttpStatus.BAD_REQUEST);
            }
        } else {
            return new ResponseEntity<>(ResponseFactory.failed("Adding post failed, invalid user data"), HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/getPost/{postId}", method = RequestMethod.GET)
    public ResponseEntity getPost(@PathVariable Long postId) {
        Optional<Post> optionalPost = Optional.ofNullable(postService.getPostById(postId));
        return optionalPost.map(post -> new ResponseEntity<>(ResponseFactory.success(post), HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(ResponseFactory.failed("Invalid post ID"), HttpStatus.BAD_REQUEST));
    }

    @RequestMapping(value = "/getAllPosts", method = RequestMethod.GET)
    public ResponseEntity<List<Post>> getAllPosts() {
        List list = postService.getAllPosts();
        if (list.size() > 0) {
            return new ResponseEntity(ResponseFactory.success(list), HttpStatus.OK);
        } else {
            return new ResponseEntity(ResponseFactory.failed("No posts available"), HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/deletePost/{postId}", method = RequestMethod.GET)
    public ResponseEntity deletePost(@PathVariable Long postId) {
        if (postService.tryToRemovePost(postId)) {
            return new ResponseEntity<>(ResponseFactory.success(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(ResponseFactory.failed("Invalid post ID"), HttpStatus.BAD_REQUEST);
        }
    }

}
