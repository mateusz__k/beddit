package com.beddit.services;

import com.beddit.models.Post;

import java.util.List;

public interface PostService {

    //chcę dodać nowy post
    boolean addNewPost(Post post);

    //chcę usunąć istniejący post
    boolean tryToRemovePost(Long postId);

    //chcę edytować post
    boolean editPost(Long postId);

    //chcę wydobyć post po id
    Post getPostById(Long postId);

    //chcę wydobyć wszystkie posty (na razie wszystkie a potem ogranicze to do konkretnego "subbeddita")
    List<Post> getAllPosts();

    //chce dodawac upvote
    boolean addUpvote(Long postId);

    //chce dodac downvote
    boolean addDownvote(Long postId);

}
