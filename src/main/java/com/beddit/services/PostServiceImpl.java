package com.beddit.services;

import com.beddit.dao.PostDao;
import com.beddit.models.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service(value = "postService")
@Transactional
public class PostServiceImpl implements PostService {

    @Autowired
    PostDao postDao;

    @Override
    public boolean addNewPost(Post post) {
        postDao.addPost(post);
        return true;
    }

    @Override
    public boolean tryToRemovePost(Long postId) {
        try {
            postDao.deletePost(postId);
            return true;
        } catch (IllegalArgumentException e) {
            return false;
        }

    }

    @Override
    public boolean editPost(Long postId) {
        postDao.update(getPostById(postId));
        return true;
    }

    @Override
    public Post getPostById(Long postId) {
        return postDao.getPostById(postId);
    }

    @Override
    public List<Post> getAllPosts() {
        return postDao.getAllPosts();
    }

    @Override
    public boolean addUpvote(Long postId) {
        Post post = getPostById(postId);
        Integer upvotes = post.getUpvotes();
        post.setUpvotes(++upvotes);
        postDao.update(post);
        return true;
    }

    @Override
    public boolean addDownvote(Long postId) {
        Post post = getPostById(postId);
        Integer upvotes = post.getUpvotes();
        post.setUpvotes(--upvotes);
        postDao.update(post);
        return true;
    }
}
