package com.beddit.services;

import com.beddit.dao.SubbedditDao;
import com.beddit.models.Post;
import com.beddit.models.Subbeddit;
import com.beddit.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.List;

@Service(value = "subbedditService")
@Transactional
public class SubbedditServiceImpl implements SubbedditService {

    @Autowired
    SubbedditDao subbedditDao;

    @Override
    public boolean addNewSubbeddit(User user, String subbedditName) {
        Subbeddit subbeddit = new Subbeddit();
        subbeddit.setName(subbedditName);
        subbeddit.setAdmins(Arrays.asList(user));   // more admins in the future
        subbedditDao.addSubbeddit(subbeddit);
        return true;
    }

    @Override
    public boolean addPostToSubbedit(Subbeddit subbeddit, Post post) {
        subbedditDao.addPostToSubbeddit(post,subbeddit);
        return true;
    }

    @Override
    public boolean addAdminToSubbeddit(Subbeddit subbeddit, User user) {
        subbedditDao.addAdminToSubbeddit(user,subbeddit);
        return true;
    }

    @Override
    public boolean deleteSubbeddit(Subbeddit subbedit) {
        subbedditDao.deleteSubbeddit(subbedit);
        return true;
    }


}

