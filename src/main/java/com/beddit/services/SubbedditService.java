package com.beddit.services;

import com.beddit.models.Post;
import com.beddit.models.Subbeddit;
import com.beddit.models.User;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public interface SubbedditService {

    boolean addNewSubbeddit(User user, String subbedditName);

    boolean addPostToSubbedit(Subbeddit subbeddit, Post post);

    boolean addAdminToSubbeddit(Subbeddit subbeddit, User user);

    boolean deleteSubbeddit(Subbeddit subbedit);


}
