package com.beddit.services;

import com.beddit.models.User;

import java.util.List;

public interface UserService {

    //chcę sprawdzić czy użytkownik o danym loginie już istnieje
    boolean isLoginUnique(String login);

    //chcę zarejestrować użytkownika
    boolean registerUser(User userToRegister);

    //chcę wylistować wszystkich użytkowników
    List<User> getAllUsers();

    //chcę zalogować użytkownika
    boolean loginUser(String login, String passwordHash);

    //chcę pobrać użytkownika po id
    User getUserById(Long userId);

    boolean removeUser(Long userId);

    boolean isEmailUnique(String email);




}
