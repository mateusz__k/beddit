package com.beddit.services;

import com.beddit.dao.UserDao;
import com.beddit.models.User;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import javax.transaction.Transactional;
import java.util.List;

@Service(value = "userService")
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    private static final Logger LOG = Logger.getLogger(UserServiceImpl.class);

    @Override
    public boolean isLoginUnique(String login) {
        LOG.debug("Checking if user " + login + " exists");
        return userDao.isUserLoginUnique(login);
        }

    @Override
    public boolean isEmailUnique(String email) {
        LOG.debug("Checking if user " + email + " exists");
        return userDao.isEmailUnique(email);
    }

    @Override
    public boolean registerUser(User userToRegister) {
        LOG.debug("Register user " + userToRegister + ".");
        userDao.addUser(userToRegister);
        return true;
    }

    @Override
    public List<User> getAllUsers() {
        LOG.debug("Returning list of all users");
        return userDao.getAllUsers();
    }

    @Override
    public boolean loginUser(String login, String passwordHash) {
        //TODO implement login method
        throw new NotImplementedException();
        //return false;
    }

    @Override
    public User getUserById(Long userId) {
        return userDao.getUserById(userId);
    }

    @Override
    public boolean removeUser(Long userId) {
        try {
            userDao.deleteUser(getUserById(userId));
            return true;
        } catch (IllegalArgumentException e) {
            return false;
        }
    }
}
