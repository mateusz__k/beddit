package com.beddit.dao;

import com.beddit.models.User;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository(value = "userDao")
public class UserDaoImpl extends AbstractDao implements UserDao {

    @Override
    public boolean isUserLoginUnique(String login) {
        Long resultCount = (Long) getSession().createCriteria(User.class)
                .add(Restrictions.eq("login", login).ignoreCase())
                .setProjection(Projections.rowCount()).uniqueResult();
        return resultCount == 0;
    }

    @Override
    public boolean isEmailUnique(String email) {
        Long resultCount = (Long) getSession().createCriteria(User.class)
                .add(Restrictions.eq("email", email).ignoreCase())
                .setProjection(Projections.rowCount()).uniqueResult();
        return resultCount == 0;
    }

    @Override
    public void addUser(User user) {
        persist(user);
    }

    @Override
    public void deleteUser(User user) {
        delete(user);
    }

    @Override
    public List getAllUsers() {
        return getAll(User.class);
    }

    @Override
    public User getUserById(Long userId) {
        return (User) getById(userId, User.class);
    }


}
