package com.beddit.dao;

import com.beddit.models.User;

import java.util.List;

public interface UserDao {

    boolean isUserLoginUnique(String login);

    void addUser(User user);

    void deleteUser(User user);

    List getAllUsers();

    User getUserById(Long userId);

    boolean isEmailUnique(String email);

}
