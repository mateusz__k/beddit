package com.beddit.dao;

import com.beddit.models.Post;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository(value = "postDao")
public class PostDaoImpl extends AbstractDao implements PostDao {

    @Override
    public void addPost(Post post) {
        if (post.getSubbeddit() != null)
            persist(post.getSubbeddit());
        persist(post);
    }

    @Override
    public void deletePost(Long postId) {
        delete(getPostById(postId));
    }

    @Override
    public List getAllPosts() {
        return getAll(Post.class);
    }

    @Override
    public Post getPostById(Long postId) {
        return (Post) getById(postId, Post.class);
    }

    @Override
    public void update(Post post) {
        getSession().save(post);
    }

}
