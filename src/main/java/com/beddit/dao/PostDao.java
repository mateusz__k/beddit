package com.beddit.dao;

import com.beddit.models.Post;

import java.util.List;

public interface PostDao {

    void addPost(Post post);

    void deletePost(Long postId);

    List getAllPosts();

    Post getPostById(Long postId);

    void update(Post post);

}
