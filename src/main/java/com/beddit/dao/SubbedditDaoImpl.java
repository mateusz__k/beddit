package com.beddit.dao;

import com.beddit.models.Post;
import com.beddit.models.Subbeddit;
import com.beddit.models.User;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository(value = "subbedditDao")
public class SubbedditDaoImpl extends AbstractDao implements SubbedditDao {

    @Override
    public void addSubbeddit(Subbeddit subbeddit) {
        persist(subbeddit);
    }

    @Override
    public void deleteSubbeddit(Subbeddit subbedit) {
        delete(subbedit);
    }

    @Override
    public List getAllSubbeddits() {
        return getAll(Subbeddit.class);
    }

    @Override
    public Subbeddit getSubbedditById(Long id) {
        return (Subbeddit) getById(id, Subbeddit.class);
    }


    @Override
    public Subbeddit getSubbedditByName(String name) {
        return null;
    }

    @Override
    public void addAdminToSubbeddit(User user, Subbeddit subbeddit) {
        List<User> admins = subbeddit.getAdmins();
        admins.add(user);
        subbeddit.setAdmins(admins);
    }

    @Override
    public void addPostToSubbeddit(Post post, Subbeddit subbeddit) {
        List<Post> posts = subbeddit.getSubbedditPosts();
        posts.add(post);
        subbeddit.setSubbedditPosts(posts);
    }


}
