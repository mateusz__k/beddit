package com.beddit.dao;

import com.beddit.models.Post;
import com.beddit.models.Subbeddit;
import com.beddit.models.User;

import java.util.List;

public interface SubbedditDao {

//    chce tworzyc nowe subbedity

    void addSubbeddit(Subbeddit subbeddit);

    //   chce usuwac subbeddity

    void deleteSubbeddit(Subbeddit subbedit);

    // chce wydobyc liste wszystkich subbedditow

    List getAllSubbeddits();

    // chce wydobyc subbeddit po id

    Subbeddit getSubbedditById(Long id);

    // chce wydobyc subbeddit po nazwie

    Subbeddit getSubbedditByName(String name);

    // chce dodac admina
    void addAdminToSubbeddit(User user, Subbeddit subbeddit);

    // chce dodac post
    void addPostToSubbeddit(Post post, Subbeddit subbeddit);




}
