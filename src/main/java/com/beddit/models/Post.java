package com.beddit.models;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
//@JsonIgnoreProperties(ignoreUnknown = true)
public class Post {

    @Id
    @GeneratedValue
    private Long id;

    private String title;

    private String body;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_date")
    private Date createDate;

    @ManyToOne
    private User user;

    @ManyToOne
    @JoinColumn(name = "subbeddit_id")
    private Subbeddit subbeddit;

    private Integer upvotes;

    @Column
    private Integer downvotes;

    public Post() {
        this.id = 0L;
    }

    public Post(String title, String body, User user, Date createDate, Subbeddit subbeddit, Integer upvotes, Integer downvotes) {
        this.id = 0L;
        this.title = title;
        this.body = body;
        this.user = user;
        this.createDate = createDate;
        this.subbeddit = subbeddit;
        this.upvotes = upvotes;
        this.downvotes = downvotes;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Subbeddit getSubbeddit() {
        return subbeddit;
    }

    public void setSubbeddit(Subbeddit subbeddit) {
        this.subbeddit = subbeddit;
    }

    public Integer getUpvotes() {
        return upvotes;
    }

    public void setUpvotes(Integer upvotes) {
        this.upvotes = upvotes;
    }

    public Integer getDownvotes() {
        return downvotes;
    }

    public void setDownvotes(Integer downvotes) {
        this.downvotes = downvotes;
    }
}
