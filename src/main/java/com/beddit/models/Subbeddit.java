package com.beddit.models;

import javax.persistence.*;
import java.util.List;

@Entity
public class Subbeddit {

    @Id
    @GeneratedValue
    @Column(name = "subbeddit_id")
    private Long id;

    private String name;

    @OneToMany
    private List<User> admins;

    @OneToMany(mappedBy = "subbeddit",cascade = CascadeType.ALL)
    private List<Post> subbedditPosts;


    public Subbeddit(String name, List<User> admins) {
        this.id = 0L;
        this.name = name;
        this.admins = admins;
    }

    public Subbeddit() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<User> getAdmins() {
        return admins;
    }

    public void setAdmins(List admins) {
        this.admins = admins;
    }

    public List<Post> getSubbedditPosts() {
        return subbedditPosts;
    }

    public void setSubbedditPosts(List<Post> subbedditPosts) {
        this.subbedditPosts = subbedditPosts;
    }
}
