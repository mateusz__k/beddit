package com.beddit.models.responses;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;

import java.time.LocalDateTime;
import java.util.UUID;

public class Response {

    private String responseUUID;

    private LocalDateTime timestamp;

    private Result result;

    Response() {
        this.responseUUID = UUID.randomUUID().toString();
        this.timestamp = LocalDateTime.now();
        this.result = new Result();
    }

    Response(Result result) {
        this.responseUUID = UUID.randomUUID().toString();
        this.timestamp = LocalDateTime.now();
        this.result = result;
    }

    public String getResponseUUID() {
        return responseUUID;
    }

    public void setResponseUUID(String responseUUID) {
        this.responseUUID = responseUUID;
    }

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "Response{" +
                "responseUUID='" + responseUUID + '\'' +
                ", time=" + timestamp +
                ", result='" + result + '\'' +
                '}';
    }
}