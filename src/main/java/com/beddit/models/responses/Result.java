package com.beddit.models.responses;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Result {

    protected Object resultObject;
    protected String message;

    public Result() {
        this.message = "OK";
    }

    public Result(String message) {
        this.message = message;
    }

    public Result(Object resultObject, String message) {
        this.resultObject = resultObject;
        this.message = message;
    }

    public Result(String reason, String message) {
        this.resultObject = null;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getResultObject() {
        return resultObject;
    }

    public void setResultObject(Object resultObject) {
        this.resultObject = resultObject;
    }
}